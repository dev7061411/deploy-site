1. Подготовить `inventory` файл описав ваш(и) хост(ы) (*+опционально способ аутентификации*)

> vim ~/Ansible/inventory

```
ans109 ansible_host=193.93.236.109 ansible_user=root ansible_ssh_private_key_file=~/.ssh/ansible
```

2. Подготовить `playbook`, что будет содержать следующие таски: 

> vim ~/Ansible/playbook.yml
```
- hosts: "all"
  tasks:
```

  * Установка `nginx`

```
  - name: "Installing nginx"
    apt:
      name: "nginx"
      update_cache: true

```

  * Копирование  конфигурации сайта (*виртуального хоста*), что была подготовлена в рамках <#1172962319446323270>

```
  - name: "Adding site to nginx"
    copy:
      src: "nginx/telefonist2.duckdns.org"
      dest: "/etc/nginx/sites-available/"

  - name: "Enabling site in nginx"
    file:
      src: "/etc/nginx/sites-available/telefonist2.duckdns.org"
      dest: "/etc/nginx/sites-enabled/telefonist2.duckdns.org"
      state: link

```

  * Копирование `SSL-сертификатов` (_из репозитория или с локальной машины_) по стандартному пути `/etc/letsencrypt`. 

```
  - name: "Copying certbot confs"
    copy:
      src: "letsencrypt"
      dest: "/etc"

```

  * Если у вас нет SSL-сертификатов (_потеряли с прошлого задания или хотите всё по новой_), то предусмотреть таску, что будет выпускать сертификат для вашего поддомена и выгружать обозначенные в `/etc/letsencrypt` ранее директории на вашу локальную машину, либо в git. Эта таска должна отрабатывать разово (_читайте про when у ansible_), чтобы она не провоцировала ошибки при повторном применении `playbook`. 

```
Есть сертификаты.
```  
  * Копирование контента сайта - тот самый `Hello World` (*или что там у вас было*). Не забывайте проверять права
```
  - name: "Copying site data to www"
    copy:
      src: "www/telefonist2.duckdns.org"
      dest: "/var/www/html/"

```
  * Проверка и создание **home** пользователя (_имя на свой вкус_), если ваш сайт лежит в `/home/*`, а также добавление его в группу `www-data`

```
Не нужно.
```
3. Доработать `playbook` так, чтобы повторная его накатка не провоцировала ошибок, а также учитывала наличие всего необходимого ПО для выполнения задачи (_certbot и пр._)
```
Не нужно. Работает из коробки.
```

4. **УДАЛИТЬ** на учебной машине пакет `nginx` через `apt purge nginx*`, чтобы убедиться, что ваш `playbook` выполняется корректно.
```
И даже rm -r /var/www/html
```
5. Применить `playbook` на удалённую машину (*на которую смотрит ваша DNS запись*)
> ~/Ansible/ansible-playbook --vault-pass-file vault-pass playbook.yml -i inventory

6. Проверить работу сайта через браузер и `curl -ILk`
```
Открывается в браузере. SSL робыть.
```

7. Зарегистрироваться в **GitLab** (либо в **GitHub**). В настройках перейти по пути `Настройки пользователя -> Ключи SSH` и добавить свой публичный `ssh-ключ`. Cоздать *ПУБЛИЧНЫЙ* репозиторий и выгрузить в него свои наработки. 
8. Подготовить отчёт (_приложив ссылку на репо_), а также коротко продублировать отчёт в свой репозиторий в файл `README.md`.
```
https://gitlab.com/dev7061411/deploy-site
```

